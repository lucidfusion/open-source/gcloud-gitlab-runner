# LF Google Cloud GitLab Runner

This Dockerfile builds a custom gcloud-specific image that allows us to access Google Container Registry and Google Kubernetes Engine from within our GitLab runner.